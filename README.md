These instructions will let you run Jenkins in your AWS account through DevPanel.

OVERVIEW: We will download and run Bitnami's Jenkins Docker container (https://github.com/bitnami/bitnami-docker-jenkins) in AWS EKS (Elastic Kubernetes Service.) We will use DevPanel "deployments" feature to do all the heavy lifting of setting up and configuring AWS for us... 

Here are the steps to set up a Jenkins deployment in DevPanel:
1. Go to the "Deployments" tab and create a "New Deployment"
1. Give your deployment a name - make it all lowercase, no spaces, dashes are ok
1. Select "Manual" for the Go Live Policy
1. Select "basic-hpa" for HELM Chart Name
1. Select the highest (largest) HELM Chart Version
1. Scroll to the bottom of the page, click on "Fill in JSON values in the form" and copy/paste the following JSON into the 
1. Copy and Paste the following into the pop-up box: 
```
{"image":{"command":[],"imagePullPolicy":"IfNotPresent","ports":[{"name":"http","containerPort":80},{"name":"services","containerPort":50000}],"env":[{"name":"JENKINS_USERNAME","value":"devpanel"},{"name":"JENKINS_PASSWORD","value":"devpanel123"},{"name":"JENKINS_HTTP_PORT_NUMBER","value":"80"}],"resources":{"requests":{"cpu":"500m","memory":"1Gi"},"limits":{"cpu":"500m","memory":"1Gi"}},"name":"jenkins","image":"docker.io/bitnami/jenkins:2"},"initContainers":[],"deployment":{"strategy":"RollingUpdate"},"podAnnotations":{},"replicaCount":1,"hpa":{"enabled":false,"maxReplicas":10}}
```
1. Under "HPA (Horizontal Pod Autoscaler)" set the maximum replicas to 1
1. Click the "Create" button
1. Once the deployment is created, select "Activate" from the Actions drop-down menu
1. Wait until the container is deployed... can take 3-5 minutes for the deployment (check Activities) + give it 3-5 minutes to spin up Jenkins
1. Click through to the auto-generated URL to visit the Jenkins login page
1. Use the default username and password (see below) to login 
1. IMPORTANT: change the default password ASAP

Here is the formatted version of the JSON for non-binary consumption:
```
{
  "image": {
    "command": [],
    "imagePullPolicy": "IfNotPresent",
    "ports": [
      { "name": "http", "containerPort": 80 },
      { "name": "services", "containerPort": 50000 }
    ],
    "env": [
      { "name": "JENKINS_USERNAME", "value": "devpanel" },
      { "name": "JENKINS_PASSWORD", "value": "devpanel123" },
      { "name": "JENKINS_HTTP_PORT_NUMBER", "value": "80" }
    ],
    "resources": {
      "requests": { "cpu": "500m", "memory": "1Gi" },
      "limits": { "cpu": "500m", "memory": "1Gi" }
    },
    "name": "jenkins",
    "image": "docker.io/bitnami/jenkins:2"
  },
  "initContainers": [],
  "deployment": { "strategy": "RollingUpdate" },
  "podAnnotations": {},
  "replicaCount": 1,
  "hpa": { "enabled": false, "maxReplicas": 10 }
}
```

PS: Note the ENV variables here: https://github.com/bitnami/bitnami-docker-jenkins; you can use any of those variables during deployment.
